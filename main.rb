start_time = Time.now


def calculate(price, quantity, state)

  states =  {
    "UT" => 1.0685,
    "CA" => 1.0825,
    "NV" => 1.08,
    "TX" => 1.0625,
    "AL" => 1.04,
  }

  raise "Invalid state. Must be of #{states.keys} " unless states.include?(state)
  raise "Price must be a number " if price.to_i < 1
  raise "Quantity must be a number " if quantity.to_i < 1

  tax_rate = states[state]
  order_value = price.to_i * quantity.to_i

  total = if order_value >= 50000
            order_value * 0.85
          elsif order_value >= 10000
            order_value * 0.90
          elsif order_value >= 7000
            order_value * 0.93
          elsif order_value >= 5000
            order_value * 0.95
          elsif order_value >= 1000
            order_value * 0.97
          else
            order_value
          end

   total * tax_rate
end




args = ARGV
puts "Processing input arguments #{args}"


puts calculate(args[0], args[1], args[2])

def test(expected, actual)
  if expected != actual
    puts "Got #{actual} but expected #{expected}"
  end


end

puts "Running tests"
puts test(calculate(1, 100, "UT"), 106.85)
puts test(calculate(1, 1000, "UT"), 1068.5 * 0.97)




